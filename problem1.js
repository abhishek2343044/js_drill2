function recall(inventory, car_id) {
  let data = inventory.filter((index) => index.id === car_id);
  console.log(
    `Car ${car_id} is a ${data[0].car_year} ${data[0].car_make} ${data[0].car_model}`
  );
}
module.exports = recall;
