function lastCar(inventory) {
  var last_id = inventory.length;

  let data = inventory.filter((index) => index.id === last_id);

  console.log(
    `Last car is a  ${data[0].car_make} ${data[0].car_model}`
  );
}
module.exports = lastCar;
