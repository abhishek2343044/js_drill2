function OldCars(inventory) {
  let old_cars = inventory.filter((index) => index < 2000);
  return old_cars;
}
module.exports = OldCars;
