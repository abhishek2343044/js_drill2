function car_models(inventory) {
  var car_model_list = inventory.map((index) => index.car_model);
  return car_model_list.sort();
}
module.exports = car_models;
