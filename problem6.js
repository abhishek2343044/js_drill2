function ListBmwAudi(inventory) {
  var BMWandAudi = inventory.filter(
    (index) => index.car_make == 'BMW' || index.car_make == 'Audi'
  );

  return BMWandAudi;
}

module.exports = ListBmwAudi;
